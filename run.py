from donationcalc import donationconfig
import donationcalc.models

donconf = donationconfig.donationconfig()
donconf.rebalance()
sender = donationcalc.models.sender()
donors = []
f = open("donors.txt", "r")

for line in f.readlines():
    linesplit = line.split(",")
    newdonor = donationcalc.models.donor(email = linesplit[0], monthly = linesplit[1])
    donors.append(newdonor)
f.close()

donationcalc.models.rebalanceall(sender,donors)
