% LUSER(1) donationcalc 1.0.0
% fram3d
% Jun 2023

# NAME
donationcalc - Web app that tracks donations and notifies users via email

# SYNOPSIS
**python3 run.py**

# DESCRIPTION
Web app that tracks donations and notifies users via email

# AUTHORS
fram3d

# COPYRIGHT
**AGPLv3+**: GNU AGPL version 3 or later <https://gnu.org/licenses/agpl.html>
This is *free* software: you are free to change and redistribute it.
There is **NO WARRANTY**, to the extent permitted by law.

