import configparser

class donationconfig():
    CONFIG_PATH = "/var/donationcalc/donationcalc/config.ini"
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    smtphost = config.get('credentials', 'SMTPHOST')
    smtpport = config.get('credentials', 'SMTPPORT')
    smtpstarttls = config.get('credentials', 'SMTPSTARTTLS')
    smtpusername = config.get('credentials', 'SMTPUSERNAME')
    smtppassword = config.get('credentials', 'SMTPPASSWORD')
    smtpaddress = config.get('credentials', 'SMTPADDRESS')

    currency = config.get('finance', 'CURRENCY')
    balance = config.get('finance', 'BALANCE')
    monthlycost = config.get('finance', 'MONTHLYCOST')

    def rebalance(self):
        newbalance = int(self.balance) - int(self.monthlycost)
        self.balance = self.config.set('finance', 'BALANCE', value = str(newbalance))

        f = open(self.CONFIG_PATH, 'w')
        self.config.write(f)
        f.close()

