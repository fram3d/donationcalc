import smtplib
from . import donationconfig

def sendmail(donor, subject, message):
    '''
        Sending mail to email of given donor with given subject and message
    '''
    config = donationconfig.donationconfig()
    smtpcon = smtplib.SMTP(str(config.smtphost), port=int(config.smtpport))

    if config.smtpstarttls:
        smtpcon.starttls()

    smtpcon.login(str(config.smtpusername), str(config.smtppassword))

    smtpcon.sendmail(config.smtpaddress, donor.email, f"From: {config.smtpaddress}\r\nTo: {donor.email}\r\nSubject: {subject}\r\n\r\n{message}")

    return 0

def rebalanceall(sender, donors = []):
    '''
        Rebalance both sender and all donors
    '''
    sender.monthlyrebalance(donors)

    for donor in donors:
        donor.monthlyrebalance()

    return 0

class sender():
    '''
        Class that represents state of donated funds and monthly costs

        balance := intiger current balance 
        cost := intiger monthly cost

    '''

    def __init__(self, balance = 'notset', monthlycost = 'notset'):

        config = donationconfig.donationconfig()

        if balance == 'notset':
            self.balance = int(config.balance)
        else:
            self.balance = int(balance)

        if monthlycost == 'notset':
            self.monthlycost = int(config.monthlycost)
        else:
            self.monthlycost = int(monthlycost)


    def monthlyrebalance(self, donors = []):
        newbalance = int(self.balance) - int(self.monthlycost)
        self.balance = newbalance

        if newbalance - self.monthlycost < 0:
            self.sendtoall(donors)
            return -1
        else:
            return 0

    def sendtoall(self, donors):

        if donors == []:
            return -1

        for donor in donors:
            sendmail(donor, "Not enough funds", "Not enough funds for next month.")

        return 0

    def pledged(self):
        sum = 0
        f = open("donors.txt", "r")

        for line in f:
            sum += int(line.split(",")[1])

        return sum

class donor():
    '''
        Class that represents a donor

        email := string email address of the donor
        monthly := string monthly donation chosen by donor
        username := string username chosen by donor
        password := string password chosen by donor
    '''

    def __init__(self, email='notset', monthly=0, username='notset', password='notset'):
        self.email = email
        self.username = username
        self.password = password
        self.monthly = monthly
        self.balance = 0

    def monthlyrebalance(self):
        newbalance = int(self.balance) - int(self.monthly)
        self.balance = newbalance

        if newbalance < 0:
            self.sendbalance()
            return -1
        else:
            return 0

    def sendbalance(self):
        notpaid = False

        if self.balance <= 0:
            sendmail(self, "Monthly donation", "You subscribed to a monthly donation of " + str(self.monthly + "."))
        else:
            sendmail(self, "Donation - Current balance", "Current balance: " + self.balance + "\nCurrent monthly donation: " + str(self.monthly))

        return 0
